# HUB / Shortcodes

## Collaborateurs list

### Departement

[collaborateurs-list endpoint='/departements/{id}' include='1' locale='{lang}']

{id} = cfr id de la DB

example : 
```
[collaborateurs-list endpoint='/departements/1' include='1' locale='fr']
```

### Cellule

[collaborateurs-list endpoint='/bu_cellules/{id}' include='1' locale='{lang}']

{id} = cfr id de la DB

example : 
```
[collaborateurs-list endpoint='/bu_cellules/1' include='1' locale='fr']
```

### organe-de-gestion

[collaborateurs-list endpoint='/organe-de-gestion/{code}' locale='{lang}']

{code} = ag,ca,cd,cc 

example : 
```
[collaborateurs-list endpoint='/organe-de-gestion/ag' locale='fr']
```

### securites

[collaborateurs-list endpoint='/securite/{code}/{etage}' locale='{lang}']

{code} = incendie,evacuation
{etage} = 1,2,3,4

example : 
```
[collaborateurs-list endpoint='/securite/incendie/3' locale='fr']
```

---

## Collaborateurs advanced search form

[collaborateurs-advanced-search-form locale='{lang}']

example : 
```
[collaborateurs-advanced-search-form locale='fr']
```